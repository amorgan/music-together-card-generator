#!/usr/bin/env python3

import csv
import copy
import reportlab
import random
import string
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.units import cm, inch
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, PageBreak, Flowable
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase.ttfonts import TTFont

# User settable variables
mom_semester            = "Fall"
mom_semester_beginning  = "Sept. 9th"
mom_collection          = "Bongos"

class flowable_fig(reportlab.platypus.Flowable):
    def __init__(self, image_path, x, y, h, w):
        reportlab.platypus.Flowable.__init__(self)
        self.img = image_path
        self.img_x = x
        self.img_y = h
        self.img_h = h
        self.img_w = w

    def draw(self):
        self.canv.drawImage(self.img, self.img_x, self.img_y, 
                width=self.img_w, height=self.img_h)

class flowable_fig(reportlab.platypus.Flowable):
    def __init__(self, image_path, x, y, h, w):
        reportlab.platypus.Flowable.__init__(self)
        self.img = image_path
        self.img_x = x
        self.img_y = h
        self.img_h = h
        self.img_w = w

    def draw(self):
        self.canv.drawImage(self.img, self.img_x, self.img_y, 
                width=self.img_w, height=self.img_h)

# Register custom fonts
pdfmetrics.registerFont(TTFont('Handwritten1', 'fonts/give_you_what_you_like.ttf'))
pdfmetrics.registerFontFamily('Handwritten1', normal='Handwritten1', bold='Handwritten1', italic='Handwritten1', boldItalic='Handwritten1')

styles=getSampleStyleSheet()
styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))

headingSpacedFar = copy.deepcopy(styles['Heading1'])
headingSpacedNear = copy.deepcopy(styles['Heading1'])
headingSpacedFar.leading = 48
headingSpacedNear.leading = 26
headingSpacedFar.fontName = 'Handwritten1'
headingSpacedNear.fontName = 'Handwritten1'

# Load csv
with open('cards.csv', 'rt') as csvfile:
    customers = csv.reader(csvfile, delimiter=',')

    # Iterate through each row/customer, then iterate through each column
    for customer in customers:
        # Get customer's family name
        cust_name = customer[0].strip()

        # Change 0 to 1 based on whichever semesters we find
        semesters = [0,0,0,
                     0,0,0,
                     0,0,0]

        # Correct any lowercase semesters
        customer[1] = customer[1].upper()

        # Skip these classes, these people just took a semester
        if 'F11' in customer[1] or 'F14' in customer[1]:
            continue

        if 'F08' in customer[1]:
            semesters[0] = 1
        if 'W15' in customer[1] or 'W12' in customer[1] or 'W09' in customer[1] \
            or 'W06' in customer[1]:
            semesters[1] = 1
        if 'S15' in customer[1] or 'S12' in customer[1] or 'S09' in customer[1] \
            or 'S06' in customer[1]:
            semesters[2] = 1
        if 'F15' in customer[1] or 'F12' in customer[1] or 'F09' in customer[1] \
            or 'F06' in customer[1]:
            semesters[3] = 1
        if 'W16' in customer[1] or 'W13' in customer[1] or 'W10' in customer[1]:
            semesters[4] = 1
        if 'S16' in customer[1] or 'S13' in customer[1] or 'S10' in customer[1]:
            semesters[5] = 1
        if 'F16' in customer[1] or 'F13' in customer[1] or 'F10' in customer[1]:
            semesters[6] = 1
        if 'W17' in customer[1] or 'W14' in customer[1] or 'W11' in customer[1]:
            semesters[7] = 1
        if 'S17' in customer[1] or 'S14' in customer[1] or 'S11' in customer[1]:
            semesters[8] = 1

        # Skip any customers that have taken all semesters
        sem_count = 0
        taken_all = True
        for sem in semesters:
            if sem == 0:
                taken_all = False
            else:
                sem_count += 1

        if taken_all:
            continue

        # Create PDF canvas to write to, start coords from top-left
        file_name = "pdfs/card-" + cust_name + '-' + "".join(
            [random.choice(string.ascii_letters) for i in xrange(3)]) + '.pdf'

        print("Generating {}".format(file_name))

        c = SimpleDocTemplate(file_name,
                rightMargin=24, leftMargin=24, bottomup=0,
                topMargin=12, bottomMargin=12, pagesize = (4*inch, 6*inch))
        content = []

        ## -------- Front Page ---------- ##
        content.append(Spacer(0, 12))

        # Add front-page image
        image_path = 'images/valentine.png'
        image_size = 188
        content.append(Image(image_path, image_size, image_size))

        content.append(Spacer(0, 38))

        text =  '<font size=44>Congratulations to the {} family!</font>'.format(
            cust_name)
        content.append(Paragraph(text, headingSpacedFar))

        content.append(PageBreak())

        ## -------- Second Page --------- ##
        content.append(Spacer(0,-80))

        # Look at semesters content and add colored pictures
        x = 0
        y = 0
        x_offset = 75
        y_offset = 30
        for i in range(len(semesters)):
            if i is not 0 and i % 3 == 0:
                # Move down one row
                y += 1
                content.append(Spacer(0, 95))

                # Go back to first column
                x = 0

            pos_x = x * x_offset + ((x - 1) * 5)
            pos_y = y * y_offset

            # Insert the image
            image_path = 'images/collection-{}'.format(i)
            if semesters[i] == 0:
                # Insert B/W image
                image_path += '-grey.png'
            else:
                # Insert colored image
                image_path += '.png'

            image = flowable_fig(image_path, pos_x, pos_y, y_offset - 120, x_offset)
            content.append(image)

            x += 1

        # Add generic text
        content.append(Spacer(0,190))

        text =  '<font size=21>Music Together offers 9 core curriculum classes ' \
            'on a 3 year rotation. A total of 205 songs to learn and share with ' \
            'your child.</font>'
        content.append(Paragraph(text, headingSpacedNear))

        content.append(PageBreak())

        ## -------- Third Page ---------- ##
        content.append(Spacer(0, 24))

        text =  '<font size=34>Your family has completed {} out of our ' \
                '9 core curriculum sessions.</font>'.format(sem_count)
        content.append(Paragraph(text, headingSpacedFar))

        content.append(Spacer(0, 12))
        text =  '<font size=34>Please join us this {} for the {} collection ' \
                'beginning {}</font>'.format(
            mom_semester, mom_collection, mom_semester_beginning)
        content.append(Paragraph(text, headingSpacedFar))

        content.append(Spacer(0, 12))
        text =  '<font size=16>www.musictogether.net / 925-551-7722</font>'
        content.append(Paragraph(text, headingSpacedNear))

        # Save and write the PDF
        c.build(content)
